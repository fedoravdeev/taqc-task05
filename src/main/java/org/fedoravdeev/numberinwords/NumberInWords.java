package org.fedoravdeev.numberinwords;

public class NumberInWords {

    private static String[] string20 = new String[]{"ноль", "один", "два", "три", "четыре", "пять", "шесть", "семь",
            "восемь", "девять", "десять", "одиннадцать", "двенадцать", "тринадцать", "четырнадцать", "пятнадцать",
            "шестнадцать", "семнадцать", "восемнадцать", "девятнадцать"};

    private static String[] string10 = new String[]{"", "десять", "двадцать", "тридцать", "сорок", "пятьдесят",
            "шестьдесят", "семьдесят", "восемьдесят", "девяносто"};

    private static String[] string100 = new String[]{"", "сто", "двести", "триста", "четыреста", "пятьсот", "шестьсот",
            "семьсот", "восемьсот", "девятьсот"};


    public static void IntToString(int i) {
        StringBuilder res = new StringBuilder();
        if ((i > 100) && (i < 1000)) {
            int t = i / 100;
            int d = t*100 - 100 - i / 10;
            int e = i % 10;
            System.out.println(t);
            System.out.println(d);
            System.out.println(e);
//            if (d == 10) {
//                res.append(string100[t]).append(" ").append(string20[e]);
//            } else {
//                res.append(string100[t]).append(" ").append(string10[d]).append(" ").append(string20[e]);
//            }
        }

        if (i < 100) {
            if (i < 20) {
                res.append(string20[i]);
            } else {
                int d = i / 10;
                int e = i % 10;
                res.append(string10[d]);
                if (e > 0) {
                    res.append(" ").append(string20[e]);
                }
            }
        }
        System.out.println(res.toString());
    }
}
